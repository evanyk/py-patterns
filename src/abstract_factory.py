# -*- coding: utf-8 -*-

import random

class Factory(object):
    """ Factory """
    def __init__(self, prototype=None):
        self.factory = prototype

    def show(self):
        p = self.factory()
        print("Show {}".format(p.say()))

class TestA(object):
    def say(self):
        return "Test A"

class TestB(object):
    def say(self):
        return "Test B"

def randomTest():
    return random.choice([TestA, TestB])()

if __name__ == "__main__":
    f = Factory(randomTest)
    f.show()
