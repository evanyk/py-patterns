# -*- coding: utf-8 -*-

"""
To understand the implementation of this pattern in Python, it is
important to know that, in Python, instance attributes are stored in a
attribute dictionary called __dict__. Usually, each instance will have
its own dictionary, but the Borg pattern modifies this so that all
instances have the same dictionary.

*Where is the pattern used practically?
Sharing state is useful in applications like managing database connections
"""

class SingleTone(object):
    __instance = {}
  
    def __init__(self):
        self.__dict__ = self.__instance
        self.state = 'Init'
    
    def __str__(self):
        return self.state

if __name__ == '__main__':
    r1 = SingleTone()
    r2 = SingleTone()

    r1.state = 'r1'
    r2.state = 'r2'

    print('r1: {0}'.format(r1))
    print('r2: {0}'.format(r2))