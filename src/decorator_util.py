from functools import wraps

"""
basic usage of wraps
"""
print("------------------------")

def w1(fn):
    @wraps(fn)
    def wrapper():
        print("setUp {}".format(fn.__name__))
        fn()
        print("tearDown {}".format(fn.__name__))
    return wrapper

@w1
def f1():
    print("f1")
    pass

f1()        


"""
basic usage of wraps (reflection)
"""
print("------------------------")

from inspect import getmembers,getargspec

def w2(fn):
    argspec = inspect.getargspec(fn)
    args = argspec[0]
    if args and args[0] == 'self':
        return argspec
    if hasattr(fn, '__func__'):
        fn = fn.__func__
    if not hasattr(fn, 'func_closure') or fn.func_closure is None:
        raise Exception("no closure for fn")
    fn = fn.func_closure[0].cell_contents
    return w2(fn)


"""
f(n) + f(n-1) + ...
"""
print("------------------------")
def memo(fn):
    cache = {}
    miss = object()

    @wraps(fn)
    def wrapper(*args):
        result = cache.get(args,miss)
        if result is miss:
            result = fn(*args)
            cache[args] = result
        return result

    return wrapper

@memo
def fib(n):
    if n < 2:
        return n
    return fib(n-1) + fib(n-2)

print("fib(10) = %d" % fib(10))


"""
url mapping
"""
print("------------------------")
class App():
    def __init__(self):
        self.func_map = {}

    def mapping(self,name):
        def fn_wrapper(fn):
            self.func_map[name] = fn
            return fn
        return fn_wrapper
    
    def call(self,name=None):
        fn = self.func_map.get(name,None)
        if fn is None:
            raise Exception("no function registered")
        return fn()

app = App()

@app.mapping('/')
def mp():
    return "this is the main page"

print( app.call('/') )