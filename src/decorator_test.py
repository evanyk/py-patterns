
def hello(fn):
    def wrapper():
        print("setUp {}".format(fn.__name__))
        fn()
        print("tearDown {}".format(fn.__name__))
    return wrapper
        
@hello
def foo():
    print("foo")

foo()

"""
decorator for class
"""
print("-----------------------------------")
class cdec(object):
    def __init__(self,fn):
        print("__init__")
        self.fn = fn
    
    def __call__(self):
        print("__call__")
        self.fn()

@cdec
def cdf():
    print("cdf")

cdf()


"""
decorator for class with parameters
"""
print("-----------------------------------")
class cdf2(object):
    def __init__(self,tag,ccls=""):
        self._tag = tag
        self._ccls = ccls

    def __call__(self,fn):
        def wrapper(*args, **kwargs):
            print("tag: {0} ccls: {1}".format(self._tag,self._ccls))
            fn(*args, **kwargs)
        return wrapper

@cdf2(tag="tag",ccls="ccls")
def c2(t,f):
    print("{0}-{1}".format(t,f))

c2("hi","deco")


"""
set parameters with decorator
"""
print("-----------------------------------")
def da(fn):
    def wrapper(*args,**kwargs):
        kwargs['str'] = 'hi'
        return fn(*args, **kwargs)
    return wrapper

@da
def pda(*args,**kwargs):
    print(kwargs['str'])

pda()



"""
set parameters with decorator
"""
print("-----------------------------------")
def dac(fn):
    def wrapper(*args,**kwargs):
        str = 'hi'
        args = args + (str,)
        return fn(*args, **kwargs)
    return wrapper

class Printer:
    @dac
    def p(self,str,*args,**kwargs):
        print(str)

p = Printer()
p.p()