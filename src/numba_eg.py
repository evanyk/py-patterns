#-*- coding: utf-8 -*-
from numba import jit 

#1
@jit("void(int32, float32)")
def foo(x,y):
    return x + y

print("#1: " , foo(1,3.2))

#2
@jit
def foo2(x,y):
    return x + y

print("#2: " , foo2(1,3))

#3
@jit(target='cpu',nopython=True)
def foo3(x,y):
    return x + y

print("#3: " , foo3(1,3))

#4
# void(f4[:], u8)	a function with no return value taking a one-dimensional array of single precision floats and a 64-bit unsigned integer.
# i4(f8)	a function returning a 32-bit signed integer taking a double precision float as argument.
# void(f4[:,:],f4[:,:])	a function with no return value taking two 2-dimensional arrays as arguments.